const fs = require('fs-extra')
const moment = require('moment')

const Jira = require('./jira')
const SETTINGS = require('./settings')

const STYLE = fs.readFileSync('./style.css')

function validateDate(log) {
    return (new Date(SETTINGS.START_DATE) <= new Date(log.started) 
        && new Date(log.started)  <= new Date(SETTINGS.END_DATE)) ? true : false;
}

function getUser(log) {
    return (log.author) ? log.author.displayName : ''
}

function nully(d) {
    return d < 10 ? '0' + d : d
}

function hasWork(task) {
    return (task.fields 
        && task.fields.timespent 
        && task.fields.worklog 
        && task.fields.worklog.worklogs 
        && task.fields.worklog.worklogs.length) ? true : false
}

function filterTasks(tasks) {
    console.log(`* tasks.length ${tasks.length}`)
    let filtered = new Array()
    tasks.forEach(task => {
        if (hasWork(task)) {
            task.fields.worklog.worklogs.forEach((log) => {
                let user = getUser(log)
                if (user && validateDate(log)) {
                    log.key = task.key
                    log.status = task.fields.status.name
                    
                    if (task.fields.assignee) {
                        log.assigned = task.fields.assignee.displayName
                    }
                    
                    if (typeof filtered[user] === 'undefined') {
                        filtered[user] = new Array()
                    }

                    filtered[user].push(log)
                }
            });
        }
    });
    return filtered
}

function countLogs(logs) {
    let period = {
        period: `${SETTINGS.START_DATE} - ${SETTINGS.END_DATE}`,
        projects: 'All',
        users: [],
        loggedHours: 0,
        numberOfTasks: 0,
        work: {},
    }
    for (var user in logs) {
        let userPeriod = {
            loggedHours: 0,
            numberOfTasks: 0,
            days: {},
        }

        logs[user].forEach((l) => {
            userPeriod.loggedHours = Number(Number(Number(userPeriod.loggedHours) 
                + Number(l.timeSpentSeconds / 60 / 60)).toFixed(2))
            userPeriod.numberOfTasks++
            let D = new Date(l.started)
            let date = `${D.getFullYear()}-${nully(D.getMonth() + 1)}-${nully(D.getDate())}`

            if (!userPeriod.days[date]) {
                userPeriod.days[date] = {
                    loggedHours: 0,
                    numberOfTasks: 0,
                    logs: [],
                };
            }
            
            userPeriod.days[date].logs.push({
                key: l.key,
                spentMinutes: l.timeSpentSeconds / 60,
                comment: l.comment,
                status: l.status,
                assigned: l.assigned,
            });

            userPeriod.days[date].loggedHours = Number(Number(userPeriod.days[date].loggedHours) 
                + Number(l.timeSpentSeconds / 60 / 60)).toFixed(2)
            userPeriod.days[date].numberOfTasks++
        });

        // sort by date
        let arr = Object.entries(userPeriod.days).sort()
        userPeriod.days = []
        
        arr.forEach((day, i) => {
            userPeriod.days[i] = day[1]
            userPeriod.days[i].date = day[0]
        })

        period.work[user] = userPeriod
        period.users.push(user)
        
        period.loggedHours += Number(userPeriod.loggedHours);
        period.numberOfTasks += userPeriod.numberOfTasks;
    }
    return period
}

function savePeriod(period) {
    fs.mkdirsSync('./output/')
    let D = new Date()
    let today = `${D.getFullYear()}-${nully(D.getMonth() + 1)}-${nully(D.getDate())}`
    fs.writeJson(`./output/from-${SETTINGS.START_DATE}-to-${SETTINGS.END_DATE}-at-${today}.json`, period)
    return
}

function withLogs(tasks) {
    let tasksWithLogs = new Array()
    tasks.forEach(t => { 
        if (t.fields.timespent) tasksWithLogs.push(t.key)
    })
    console.log('tasksWithLogs.length', tasksWithLogs.length)
    return tasksWithLogs
}

function fillDays(startDate, endDate) {
    console.log('startDate', startDate)
    function nextDate(date) {
        let next = moment(date).add(1, 'days').format('YYYY-MM-DD')
        console.log('next', next)
        days.push(next)
        if (next === endDate) {
            console.log('next', next)
            console.log('endDate', endDate)
            return
        } else {
            nextDate(next)
        }
    }
    let days = []
    days.push(startDate)
    nextDate(startDate)
    return days
}

function makeHTML(period) {
    let HTML = `
<style>
    ${STYLE}
</style>
<table>
    <tr><th>Person</th><th>Total, h</th>`
    let startDate = SETTINGS.START_DATE
    let endDate = SETTINGS.END_DATE

    let days = fillDays(startDate, endDate)
    console.log('days', days)
    days.forEach(day => {
        HTML += `<th>${day}</th>`
    })

    period.users.forEach(user => {
        HTML += `
        <tr><td>${user}</td>`
        let total = 0
        period.work[user].days.forEach(day => {
            total += Number(day.loggedHours)
        })

        HTML += `<td>${total.toFixed(2)}</td>`
        days.forEach(day => {
            // console.log(`
            // day`, day)
            // console.log(`period.work[${user}].days`, period.work[user].days)
            let work = period.work[user].days.find(d => d.date === day)
            // console.log('work', work)
            let logged = (work) ? work.loggedHours : 0
            // console.log('logged', logged)

            let color = 'lightgreen'
            if (logged < 6) {
                color = 'aqua'
            } 
            if (logged > 8) {
                color = 'orange'
            } 
            if (logged > 9) {
                color = 'red'
            }
            if (logged === 0) {
                color = 'white'
            }

            HTML += `<td style="background: ${color}">`
            HTML += `${logged}`
            HTML += `</td>`
        })
        //
        HTML += `</tr>`
    })
    HTML += `
    </table`

    fs.mkdirsSync('./output/')
    let D = new Date()
    let today = moment().format('YYYY-MM-DD')
    fs.writeFileSync(`./output/from-${startDate}-to-${endDate}-at-${today}.html`, HTML)
    return
}

function saveJiraTasks(tasks) {
    fs.mkdirsSync('./output/')
    let D = new Date()
    let today = moment().format('YYYY-MM-DD')
    fs.writeJSONSync(`./output/tasks-${today}.json`, tasks)
    return tasks
}

function makeReport() {
    console.log('makeReport started')
    Jira.search(`updated >= ${SETTINGS.START_DATE}`)
        .then(saveJiraTasks)
        .then(withLogs)
        .then(Jira.getTasks)
        .then(filterTasks)
        .then(countLogs)
        .then(period => {
            console.log(`From ${SETTINGS.START_DATE} to ${SETTINGS.END_DATE}
                spent ${Number(period.loggedHours).toFixed(2)} hours`)

            savePeriod(period)

            return period
        })
        .then(makeHTML)
}

// prepare and save in json file
// full cicle
// makeReport()

// short cicle
const period = require('./output/from-2020-11-01-to-2020-11-30-at-2020-11-24.json')
makeHTML(period)

/*
 * Notes:
 * Need to make extension with injection on btb jira like widget
 */