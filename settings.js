/* 
 * SETTINGS
 */
const moment = require('moment')

const SETTINGS = {
	JIRA: {
		headers: {
			headers: {
				'Authorization': `Basic YWxleGV5dUBzb2Z0b21hdGUuY29tOnhPdm5GUGtteEFwNHFkelVWUVBGREI2Rg==`,
				'Content-Type': 'application/json'
			}
		},
		home: 'https://softomate.atlassian.net',
	},
	teams: [
		{
			"name": "pandas", 
			"users": [
				"alexeyu@softomate.com",
				"anton.istomin@softomate.com",
				"kraev@softomate.com",
				"denis.inozemtsev@softomate.com",
				"igorjrm@softomate.com",
				"pavel.sasin@softomate.com"
			],
			"channel": 
			"https://hooks.slack.com/services/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"
		},
		{
			"name": "best-team", 
			"users": [
				"oleg.usmanov@softomate.com", 
				"alima.troitskaya@softomate.com", 
				"vladislav.postnykh@softomate.com", 
				"artem.andrianov@softomate.com"
			], 
			"channel": "https://hooks.slack.com/services/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"
		}
	],
  START_DATE: '2020-11-01',
  END_DATE: '2020-11-30',
}

module.exports = SETTINGS
